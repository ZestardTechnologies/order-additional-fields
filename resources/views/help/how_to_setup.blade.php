<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <p data-toggle="collapse" data-parent="#accordion" href="#collapse9"> 
                <strong><span class="">Global Configuration Settings</span>
                    <span class="fa fa-chevron-down pull-right"></span></strong>
            </p>
        </h4>
    </div>
    <div id="collapse9" class="panel-collapse collapse">
        <div class="panel-body">
            <div class ="row">
                <div class ="col-sm-6">
                    <ul class="ul-help">
                        <ul>
                            <li>Click on <b>Global Configuration</b> Tab and set the fields data according to requirement.</li>
                            <li>You can change the format of <b>Date</b>, <b>Time</b> and <b>Date-Time</b> fields using the Global Configuration. Once you change the format from Global Configuration, the changes will be reflected in Edit Collection page.</li>
                            <li>You can easily <b>Enable</b> or <b>Disable</b> App, Set <b>Additional Title</b> and <b>Additional CSS</b> for look and feel of your store front side from <b>Global Configuration</b> Setting</li>
                        </ul>
                    </ul>
                </div>
                <div class ="col-sm-6">
                    <div class ="screenshot_box">
                        <a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/help/help_001.png') }}" target="_blank">
                            <img class="img-responsive" src="{{ asset('image/help/help_001.png') }}">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <p data-toggle="collapse" data-parent="#accordion" href="#collapse1"> 
                <strong><span class="">Setup Your Application</span>
                    <span class="fa fa-chevron-down pull-right"></span></strong>
            </p>
        </h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse">
        <div class="panel-body">
            @include('help.how_to_setup_step1')
            @include('help.how_to_setup_step2')
        </div>
    </div>
</div>