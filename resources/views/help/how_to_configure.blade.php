<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <p data-toggle="collapse" data-parent="#accordion" href="#collapse2"> 
                <strong><span class="">Pasting Shortcodes</span>
                    <span class="fa fa-chevron-down pull-right"></span></strong>
            </p>
        </h4>
    </div>

    <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body">
            @include('help.how_to_configure_step1')
            @include('help.how_to_configure_step2')
        </div>
    </div>
</div>