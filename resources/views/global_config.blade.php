@extends('header')
@section('content')
<?php $store_name = session('shop'); ?>
<!--<div class="container formcolor formcolor_globalconfig" >-->
<div class="formcolor formcolor_globalconfig" >
    <div class=" row">
        <div class="globalconfig_page">
            <div id="wrap">
                <div class="col-md-12 col-xs-12" style="background:#ebeef0">
                    <h1 style="font-size: 20px;color: #697882;font-weight: 400;padding-left: 15px;">
                    </h1>
                    <div class="panel-body" style="background-color:#fff;">
                        <form action="{{ url('save_form_global_config') }}" name="saveform" class="custom-form-design" style="border: 1px solid #ccc;padding: 20px;" method="post" accept-charset="utf-8">
                            {!! csrf_field() !!}
                            <div class="main-col-inner">
                                <!--<h1 class="panel-heading" style="font-size: 24px;width: 100%;">Meta Fields Configuration</h1>-->
                                <legend class="info-head">Meta Fields Configuration</legend>
                                <div class="entry-edit">
                                    <div class="section-config panel-body">
                                        <!--<legend class="info-head">Date &amp; Time Options</legend>-->
                                        <table class="table table-inverse">
                                            <colgroup></colgroup>
                                            <colgroup class="value"></colgroup>
                                            <tbody style="background:#ebeef0">                
                                                <tr>
                                                    <td>
                                                        <label for="date_field_order"> Date Fields Order</label>
                                                    </td>
                                                    <td class="value" id="date_field_order">
                                                        <?php $date_field_order = ($data[0] != NULL && $data[0]->date_field_order != '') ? unserialize($data[0]->date_field_order) : array(); ?>
                                                        <select name="date_field_order[0]">
                                                            <option <?php echo ($data[0] != NULL && $date_field_order[0] == 'd') ? 'selected' : ''; ?> value="d">Day</option>
                                                            <option  <?php echo ($data[0] != NULL && $date_field_order[0] == 'm') ? 'selected' : ''; ?> value="m">Month</option>
                                                            <option  <?php echo ($data[0] != NULL && $date_field_order[0] == 'y') ? 'selected' : ''; ?> value="y">Year</option>
                                                        </select>
                                                        <span>/</span>
                                                        <select name="date_field_order[1]">
                                                            <option <?php echo ($data[0] != NULL && $date_field_order[1] == 'd') ? 'selected' : ''; ?> value="d">Day</option>
                                                            <option  <?php echo ($data[0] != NULL && $date_field_order[1] == 'm') ? 'selected' : ''; ?> value="m">Month</option>
                                                            <option  <?php echo ($data[0] != NULL && $date_field_order[1] == 'y') ? 'selected' : ''; ?> value="y">Year</option>
                                                        </select>
                                                        <span>/</span>
                                                        <select name="date_field_order[2]">
                                                            <option <?php echo ($data[0] != NULL && $date_field_order[2] == 'd') ? 'selected' : ''; ?> value="d">Day</option>
                                                            <option  <?php echo ($data[0] != NULL && $date_field_order[2] == 'm') ? 'selected' : ''; ?> value="m">Month</option>
                                                            <option  <?php echo ($data[0] != NULL && $date_field_order[2] == 'y') ? 'selected' : ''; ?> value="y">Year</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label for="time_format"> Time Format</label>
                                                    </td>
                                                    <td class="value">
                                                        <select id="atime_format" name="time_format" class="form-control">
                                                            <option <?php echo ($data[0] != NULL && $data[0]->time_format == '1') ? 'selected' : ''; ?> value="1">12h AM/PM</option>
                                                            <option <?php echo ($data[0] != NULL && $data[0]->time_format == '2') ? 'selected' : ''; ?> value="2">24h</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label for="app_status">Enable / Disable App</label>
                                                    </td>
                                                    <td class="value" id="app_status">
                                                        <select name="app_status" id="app_status" class="form-control">
                                                            <option  <?php echo ($data[0] != NULL && $data[0]->app_status == '1') ? 'selected' : ''; ?> value="1">Enable</option>
                                                            <option <?php echo ($data[0] != NULL && $data[0]->app_status == '0') ? 'selected' : ''; ?> value="0">Disable</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>
                                                        <label for="additional_title">Additional Title</label>
                                                    </td>
                                                    <td class="value" id="additional_title">
                                      <input type="text" name="additional_title" id="additional_title" class="form-control" value="<?php echo $data[0]->additional_title; ?>">
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>
                                                        <label for="additional_css">Additional CSS</label>
                                                    </td>
                                                    <td class="value" id="additional_css">
                                                        <textarea name="additional_css" class="form-control" rows="10"><?php echo $data[0]->additional_css; ?></textarea>
                                                    </td>
                                                </tr>
                                                

                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>

                            <div style="margin-top: 20px;">
                                <input type="submit" name="submit" value="Save Changes" id="BtnSaveGlobalConfig" class="btn btn-primary submitform">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#BtnSaveGlobalConfig").on('click', function (event) {
            startloader(1);
        });
    });
    function startloader(process) {
        if (process == 1) {
            $(".overlay").css({
                'display': 'block',
                'background-image': 'url({{ asset("image/loader.gif") }})',
                'background-repeat': 'no-repeat',
                'background-attachment': 'fixed',
                'background-position': 'center'
            });
        } else {
            $(".overlay").css({
                'display': 'none',
                'background-image': 'none',
            });
        }
    }
</script>
@endsection