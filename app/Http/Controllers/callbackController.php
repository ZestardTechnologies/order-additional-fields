<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\ShopModel;

class callbackController extends Controller {

    var $headers = "";

    public function __construct() {
        $this->headers = 'MIME-Version: 1.0' . "\r\n";
        $this->headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    }

    public function index(Request $request) {

        $sh = App::make('ShopifyAPI');

        $app_settings = DB::table('appsettings')->where('id', 1)->first();

        if (!empty($_GET['shop'])) {
            $shop = $_GET['shop'];

            $select_store = DB::table('usersettings')->where('store_name', $shop)->get();

            if (count($select_store) > 0) {
                //session(['shop' => $shop]);
                //Remove comment for the Payment method
                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
                $id = $select_store[0]->charge_id;
                $url = 'admin/recurring_application_charges/' . $id . '.json';
                $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);
                $charge_id = $select_store[0]->charge_id;
                $charge_status = $select_store[0]->status;
                if (!empty($charge_id) && $charge_id > 0 && $charge_status == "active") {
                    session(['shop' => $shop]);
                    return redirect()->route('globalconfig');
                } else {
                    return redirect()->route('payment_process');
                }
            } else {
                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop]);
                $permission_url = $sh->installURL(['permissions' => array('read_script_tags', 'write_script_tags', 'read_customers', 'write_customers', 'read_products', 'write_products', 'read_content', 'write_content', 'read_orders', 'write_orders', 'read_draft_orders', 'write_draft_orders'), 'redirect' => $app_settings->redirect_url]);
                return redirect($permission_url);
            }
        }
    }

    public function redirect(Request $request) {
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        if (!empty($request->input('shop')) && !empty($request->input('code'))) {
            $shop = $request->input('shop'); //shop name

            $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
            if (count($select_store) > 0) {
                //session(['shop' => $shop]);                
                //Remove coment for the Payment method
                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
                $id = $select_store[0]->charge_id;
                $url = 'admin/recurring_application_charges/' . $id . '.json';
                $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);
                $charge_id = $select_store[0]->charge_id;
                $charge_status = $select_store[0]->status;
                if (!empty($charge_id) && $charge_id > 0 && $charge_status == "active") {
                    session(['shop' => $shop]);
                    return redirect()->route('globalconfig');
                } else {
                    return redirect()->route('payment_process');
                }
            }

            $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop]);
            try {
                $verify = $sh->verifyRequest($request->all());
                if ($verify) {
                    $code = $request->input('code');
                    $accessToken = $sh->getAccessToken($code);
                    DB::table('usersettings')->insert(['access_token' => $accessToken, 'store_name' => $shop, 'store_encrypt' => ""]);
                    $shop_find = ShopModel::where('store_name', $shop)->first();
                    $shop_id = $shop_find->id;

                    $encrypt_id = crypt($shop_id, "ze");
                    $finaly_encrypt = str_replace(['/', '.'], "Z", $encrypt_id);

                    DB::table('usersettings')->where('id', $shop_id)->update(['store_encrypt' => $finaly_encrypt]);

                    DB::table('zestard_global_config')->insert(['store_id' => $shop_id, 'date_field_order' => 'a:3:{i:0;s:1:"d";i:1;s:1:"m";i:2;s:1:"y";}', 'time_format' => '1', 'app_status' => '1', 'additional_css' => '', 'additional_title' => '']);

                    $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);

                    //for creating the uninstall webhook
                    $url = 'https://' . $_GET['shop'] . '/admin/webhooks.json';
                    $webhookData = [
                        'webhook' => [
                            'topic' => 'app/uninstalled',
                            'address' => config('app.url') . 'uninstall.php',
                            'format' => 'json'
                        ]
                    ];
                    $uninstall = $sh->appUninstallHook($accessToken, $url, $webhookData);

                    //api call for creating the app script tag
                    $script_url = "https://zestardshop.com/shopifyapp/order-additional-fields/public/js/zestard_metafields.js";
                    $script = $sh->call(['URL' => '/admin/script_tags.json', 'METHOD' => 'POST', 'DATA' => ['script_tag' => ['event' => 'onload', 'src' => $script_url, 'display_scope' => 'all']]]);
                    session(['shop' => $shop]);
                    //creating the Recuring charge for app
                    $url = 'https://' . $shop . '/admin/recurring_application_charges.json';
                    if ($shop == "free-theme-test.myshopify.com") {
                    $charge = $sh->call([
                        'URL' => $url,
                        'METHOD' => 'POST',
                        'DATA' => array(
                            'recurring_application_charge' => array(
                                'name' => 'Order Additional Fields',
                                'price' => 4.99,
                                'return_url' => url('payment_success/'.$shop),
                                /* 'capped_amount' => 20, */
                                'terms' => 'Terms & Condition Applied',
                                'test' => true
                            )
                        )
                            ], false);
                    }else {
                        
                        $charge = $sh->call([
                        'URL' => $url,
                        'METHOD' => 'POST',
                        'DATA' => array(
                            'recurring_application_charge' => array(
                                'name' => 'Order Additional Fields',
                                'price' => 4.99,
                                'return_url' => url('payment_success/'.$shop),
                                /* 'capped_amount' => 20, */
                                'terms' => 'Terms & Condition Applied',
                                //'test' => true
                            )
                        )
                            ], false);
                        
                    }

                    $create_charge = DB::table('usersettings')->where('store_name', $shop)->update(['charge_id' => (string) $charge->recurring_application_charge->id, 'api_client_id' => $charge->recurring_application_charge->api_client_id, 'price' => $charge->recurring_application_charge->price, 'status' => $charge->recurring_application_charge->status, 'billing_on' => $charge->recurring_application_charge->billing_on, 'payment_created_at' => $charge->recurring_application_charge->created_at, 'activated_on' => $charge->recurring_application_charge->activated_on, 'trial_ends_on' => $charge->recurring_application_charge->trial_ends_on, 'cancelled_on' => $charge->recurring_application_charge->cancelled_on, 'trial_days' => $charge->recurring_application_charge->trial_days, 'decorated_return_url' => $charge->recurring_application_charge->decorated_return_url, 'confirmation_url' => $charge->recurring_application_charge->confirmation_url, 'domain' => $shop]);
                    $shopi_info = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);
                    //mail for the installation of app
                    $msg = '<table>
                            <tr>
                                <th>Shop Name</th>
                                <td>' . $shopi_info->shop->name . '</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>' . $shopi_info->shop->email . '</td>
                            </tr>
                            <tr>
                                <th>Domain</th>
                                <td>' . $shopi_info->shop->domain . '</td>
                            </tr>
                            <tr>
                                <th>Phone</th>
                                <td>' . $shopi_info->shop->phone . '</td>
                            </tr>
                            <tr>
                                <th>Shop Owner</th>
                                <td>' . $shopi_info->shop->shop_owner . '</td>
                            </tr>
                            <tr>
                                <th>Country</th>
                                <td>' . $shopi_info->shop->country_name . '</td>
                            </tr>
                            <tr>
                                <th>Plan</th>
                                <td>' . $shopi_info->shop->plan_name . '</td>
                            </tr>
                          </table>';
                      mail("vijay.zestard@gmail.com", "Order Additional Fields Installed", $msg, $this->headers);
                    $store_details = DB::table('development_stores')->where('dev_store_name', $shop)->first();                       
                    if(count($store_details) <= 0){
                        mail("vijay.zestard@gmail.com", "Order Additional Fields Installed", $msg, $this->headers);
                        mail("support@zestard.com", "Order Additional Fields Installed", $msg, $this->headers);
                    }
                    //redirecting to the Shopify payment page
                    echo '<script>window.top.location.href="' . $charge->recurring_application_charge->confirmation_url . '"</script>';
                } else {
                    // Issue with data
                }
            } catch (Exception $e) {
                echo '<pre>Error: ' . $e->getMessage() . '</pre>';
            }
        }
    }

    public function dashboard() {
        $shop = session('shop');
        $id = DB::table('usersettings')->where('store_name', $shop)->value('id');

        return view('dashboard');
    }

    /* Payment Process */

    public function payment_method(Request $request) {
        $shop = session('shop');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        if (count($select_store) > 0) {
            $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);

            $charge_id = $select_store[0]->charge_id;
            $url = 'admin/recurring_application_charges/' . $charge_id . '.json';
            $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);

            if (count($charge) > 0) {
                if ($charge->recurring_application_charge->status == "pending") {
                    echo '<script>window.top.location.href="' . $charge->recurring_application_charge->confirmation_url . '"</script>';
                } elseif ($charge->recurring_application_charge->status == "declined" || $charge->recurring_application_charge->status == "expired") {

                    //creating the new Recuring charge after declined app
                    $url = 'https://' . $shop . '/admin/recurring_application_charges.json';
                    $charge = $sh->call([
                        'URL' => $url,
                        'METHOD' => 'POST',
                        'DATA' => array(
                            'recurring_application_charge' => array(
                                'name' => 'Order Additional Fields',
                                'price' => 4.99,
                                'return_url' => url('payment_success/'.$shop),
                                /* 'capped_amount' => 20, */
                                'terms' => 'Terms & Condition Applied',
                                //'test' => true
                            )
                        )
                            ], false);

                    $create_charge = DB::table('usersettings')->where('store_name', $shop)->update(['charge_id' => (string) $charge->recurring_application_charge->id, 'api_client_id' => $charge->recurring_application_charge->api_client_id, 'price' => $charge->recurring_application_charge->price, 'status' => $charge->recurring_application_charge->status, 'billing_on' => $charge->recurring_application_charge->billing_on, 'payment_created_at' => $charge->recurring_application_charge->created_at, 'activated_on' => $charge->recurring_application_charge->activated_on, 'trial_ends_on' => $charge->recurring_application_charge->trial_ends_on, 'cancelled_on' => $charge->recurring_application_charge->cancelled_on, 'trial_days' => $charge->recurring_application_charge->trial_days, 'decorated_return_url' => $charge->recurring_application_charge->decorated_return_url, 'confirmation_url' => $charge->recurring_application_charge->confirmation_url, 'domain' => $shop]);

                    //redirecting to the Shopify payment page
                    echo '<script>window.top.location.href="' . $charge->recurring_application_charge->confirmation_url . '"</script>';
                } elseif ($charge->recurring_application_charge->status == "accepted") {

                    $active_url = '/admin/recurring_application_charges/' . $charge_id . '/activate.json';
                    $Activate_charge = $sh->call(['URL' => $active_url, 'METHOD' => 'POST', 'HEADERS' => array('Content-Length: 0')]);
                    $Activatecharge_array = get_object_vars($Activate_charge);
                    $active_status = $Activatecharge_array['recurring_application_charge']->status;
                    $update_charge_status = DB::table('usersettings')->where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $active_status]);
                    return redirect()->route('globalconfig');
                }
            }
        }
    }

    /* Payment Success */

    public function payment_compelete(Request $request,$shop) {
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        //$shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();

        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        $charge_id = $_GET['charge_id'];
        $url = 'admin/recurring_application_charges/#{' . $charge_id . '}.json';
        $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET',]);
        $status = $charge->recurring_application_charges[0]->status;

        $update_charge_status = DB::table('usersettings')->where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $status]);
        if ($status == "accepted") {
            $active_url = '/admin/recurring_application_charges/' . $charge_id . '/activate.json';
            $Activate_charge = $sh->call(['URL' => $active_url, 'METHOD' => 'POST', 'HEADERS' => array('Content-Length: 0')]);
            $Activatecharge_array = get_object_vars($Activate_charge);
            $active_status = $Activatecharge_array['recurring_application_charge']->status;
            $update_charge_status = DB::table('usersettings')->where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $active_status]);
            return redirect()->route('globalconfig');
        } elseif ($status == "declined") {
            return redirect()->route('decline');
            //echo '<script>window.top.location.href="https://'.$shop.'/admin/apps"</script>';
        }
    }

    public function declined(Request $request) {
        $shop = session('shop');
        echo '<script>window.top.location.href="https://' . $shop . '/admin/apps"</script>';
    }

}
