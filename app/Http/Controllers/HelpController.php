<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use DB;

class HelpController extends Controller {
    
    public function index()
    {
        $app_type = "Orders";
        $app_type_singular = "Order";
        $dashboard_route = "/shopifyapp/order-additional-fields/public/orders";
        $liquid_file_path = "/admin/themes/current/?key=templates/customers/order.liquid";
        $main_shortcode = '<div id="metafields" class="additional_css">&#13;&#10;<h2 class="additional_title"></h2>&#13;&#10;<div class="multi_custom_orders" id="{{order.id}}"></div></div></div>&#13;&#10;<div class="custom_orders_fields" id="shortcode_order_appname_title"><span>[label]:</span><span>[value]</span></div>&#13;&#10;</div>';
        $field_shortcode = '<div class="custom_orders_fields" id="shortcode_order_appname_title"><span>[label]:</span><span>[value]</span></div>';
        return view('help.help', compact('dashboard_route' , 'app_type' ,'app_type_singular','liquid_file_path','main_shortcode','field_shortcode'));
    }
    
    public function appConfiguration()
    {
        $app_type = "Orders";
        $app_type_singular = "Order";
        $dashboard_route = "/shopifyapp/order-additional-fields/public/orders";
        $liquid_file_path = "/admin/themes/current/?key=templates/customers/order.liquid";
        $main_shortcode = '<div id="metafields" class="additional_css">&#13;&#10;<h2 class="additional_title"></h2>&#13;&#10;<div class="multi_custom_orders" id="{{order.id}}"></div></div></div>&#13;&#10;<div class="custom_orders_fields" id="shortcode_order_appname_title"><span>[label]:</span><span>[value]</span></div>&#13;&#10;</div>';
        $field_shortcode = '<div class="custom_orders_fields" id="shortcode_order_appname_title"><span>[label]:</span><span>[value]</span></div>';
        return view('app_configuration',compact('dashboard_route' , 'app_type' ,'app_type_singular','liquid_file_path','main_shortcode','field_shortcode'));
    }
}